# Fish Quiz

This algorithm is created in order to solve a puzzle.
Puzzle contains 9 square pieces with combination of heads/tails on each side. To solve this puzzle you must connect all pieces so each side will match.

# Algorithm

This program uses recursive algorithm to work throuh all possible combinations and finds 4 results in total (same results with 4 different rotations).


## Authors

* **Fred Martmaa** 

