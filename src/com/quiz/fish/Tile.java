package com.quiz.fish;

import java.util.ArrayList;
import java.util.List;

public class Tile {
	
	public static final int TOTAL_ROTATIONS = 4;
	private int rotations;
	
	public List<Tile> promisingTiles = new ArrayList<>();
	
	private int number;
	public Combination top;
	public Combination right;
	public Combination bottom;
	public Combination left;
	
	public Tile north;
	public Tile east;
	public Tile south;
	public Tile west;
	
	public Tile(int number, Combination top, Combination right, Combination bottom, Combination left){
		this.number = number;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.left = left;
		rotations = TOTAL_ROTATIONS;
	}
	
	public int getNumber(){
		return number;
	}
	
	public int getRotations(){
		return rotations;
	}

	public boolean canInsert(Tile tile, Tile[][] board, int row, int col) {
		
		boolean northOk = false,eastOk = false,southOk = false,westOk = false;
		
		// check north
		if(row <= 0){
			northOk = true;
		} else {
			north = board[row - 1][col];
			
			if(north == null){
				northOk = true;
			} else {
				
				if(tile.top.getCombination() == north.bottom.getMatch()){
					northOk = true;
				} else {
					northOk = false;
				}
				
			}
		}

		
		// check east
		if(col < Board.WIDTH){
			eastOk = true;
		} else {
			east = board[row][col + 1];
			
			if(east == null){
				eastOk = true;
			} else {
				
				if(tile.right.getCombination() == east.left.getMatch()){
					eastOk = true;
				} else {
					eastOk = false;
				}
				
			}
		}	
		
		// check south
		if(row < Board.HEIGHT){
			southOk = true;
		} else {
			south = board[row + 1][col];
			
			if(south == null){
				southOk = true;
			} else {
				
				if(tile.bottom.getCombination() == south.top.getMatch()){
					southOk = true;
				} else {
					southOk = false;
				}	
			}
		}
	
		
		// if OOB then set westOk = TRUE
		if(col <= 0){
			westOk = true;
		} else {
			west = board[row][col - 1];

			if(west == null || tile.left.getCombination() == board[row][col - 1].right.getMatch()){
				west = board[row][col - 1];
				westOk = true;
			} else {
				westOk = false;
			}
		}
		
		if(northOk && eastOk && southOk && westOk){
			
			System.out.println("inserted #"+number+" in ["+row+"]["+col+"]");
			return true;
		} else {
			System.out.println("#" + number + " -> " + northOk + " " + eastOk + " " + southOk + " " + westOk);
		}

		return false;
	}
	

	private boolean canRotate(){
		return rotations > 0;
	}
	
	public void initRotations(){
		System.out.println("init rotations for #" + number);
		rotations = TOTAL_ROTATIONS;
	}
	
	public boolean rotate(){
		System.out.println("Rotating -> #" + number + " (" + rotations + ")");
		if(!canRotate()){
			System.out.println("#" + number + " is out of rotations");
			return false;
		}
		
		Combination tempTile = top;
		
		top = left;
		left = bottom;
		bottom = right;
		right = tempTile;
		
		rotations--;
				
		return true;
	}
	
	
	
	@Override
	public String toString(){
		return "#"+number + " ["+ top + " - " + right + " - " + bottom + " - " + left + "]";
	}


	
}
