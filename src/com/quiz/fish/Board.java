package com.quiz.fish;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Board {
	public static final int WIDTH = 3;
	public static final int HEIGHT = 3;
	
	public Tile[][] board = new Tile[WIDTH][HEIGHT];
	public List<Tile> tiles = new ArrayList<>();
	public List<Integer> result = new ArrayList<>();
	
	int recursion_counter = 0;
	int combinations = 0;
	
	public List<List<Integer>> allResults = new ArrayList<>();
	public Stack<Integer> st = new Stack<>();
	
	public List<Result> r = new ArrayList<>();
		
	// EXAMLE RESULT [2, 9, 5, 3, 6, 7, 1, 8, 4]
	public void start() throws InterruptedException{
		
		createTiles();
		
		int counter = 0;
		
		// loop through all tiles.
		while(counter < tiles.size()){
			
			int find = counter;
			
			List<Tile> searchList = new ArrayList<>(tiles);
			List<Integer> resultList = new ArrayList<>(result);
			
			Tile first = searchList.get(find); // start from index 0
			searchList.remove(first); // remove first element since we start from it
			
			first.initRotations();
			
			board = new Tile[WIDTH][HEIGHT];
			
			// Recursive method. 
			// Always returns false since we need all possible variations.
			if(solve(board, searchList, first, resultList)){
				System.out.println("Found solution -> " + st);
				Thread.sleep(3000);
			}
			
			counter++;
		}
		
		// In future this should be replaced with recursion ending condition
		// and result should be printed in Result class.
		System.out.println("\n\n-------------------------------");
		for(int i=0; i<Result.resultArray.size(); i++){
			System.out.println("\n" + Result.resultArray.get(i));
		}
		System.out.println("\n-------------------------------");
		System.out.println("combinations > " + combinations);
		System.out.println("recursion counts > " + recursion_counter);
		
	}
	
	public boolean solve(Tile[][] board, List<Tile> tileList, Tile tile, List<Integer> resultList) throws InterruptedException{

		recursion_counter++;
		int[] coordinates = findNextEmptyTile(board);
		
		
		int row = coordinates[0];
		int col = coordinates[1];
		
		System.out.println("\n-----------------------------------------");
		System.out.println("solve: @coordinates: board[" + row + "][" + col + "]");
		System.out.println("@Tile: " + tile + " (" + tile.getRotations() + ")");
		System.out.println("@searchList: " + tileList);
		// System.out.println("@resultList: " + resultList);
		System.out.println("@Stack -> " + st);
		System.out.println("-----------------------------------------\n");
		
		
		// board is full, we are done
		if(isFilled()){
			System.out.println(resultList);
			System.out.println("Board is full");
			Thread.sleep(2000);
			return true;
		}
		
		// as long as we can rotate tile
		while(tile.rotate()){

			resultList.remove(tile);
			
			// try to insert it in the board
			if(isSafe(board, row, col, tile)){
				
				board[row][col] = tile;
				resultList.add(tile.getNumber());
				st.push(tile.getNumber());
				
				for(int i=0; i<tileList.size(); i++){
					
					Tile next = tileList.get(i);
					List<Tile> childTiles = new ArrayList<>(tileList);
					
					// initialize rotations for all child tiles
					for(Tile t: childTiles){
						t.initRotations();
					}
					
					List<Integer> newResultList = new ArrayList<>(resultList);
					childTiles.remove(next);
					
					if(solve(board, childTiles, next, newResultList)){
						System.out.println("Solved for next -> #" + next.getNumber());
						return true;
					}
					
					newResultList.remove(next);
					
				}
				
				if(st.size() == 9){
					System.out.println(st);
					Stack<Integer> result = new Stack<>();
					result.addAll(st);
					
					@SuppressWarnings("unused")
					Result r = new Result(result);
				}
				
				st.pop();
				board[row][col] = null;
			}
		}

		return false;
	}
	
	public boolean isFilled(){
		return board[2][2] != null;
	}
	
	public boolean isSafe(Tile[][] board, int row, int col, Tile tile){
		combinations++;
		return tile.canInsert(tile, board, row, col);
	}
	
	public int[] findNextEmptyTile(Tile[][] board){
		
		int[] result = new int[2];
		
		for(int row = 0; row < Board.WIDTH; row++){	
			for(int col = 0; col < Board.HEIGHT; col++){
				if(board[row][col] == null){
					
					result[0] = row;
					result[1] = col;
										
					return result;
				}
			}
		}
		
		return null;
	}


	/*
	 * Method that creates 9 Tiles matching the original puzzle pieces.
	 * Each side represents a combination of heads/tails.
	 * Each tile has 4 sides with combinations.
	 * Each tile has a number so we can track the result.
	 */
	public void createTiles(){
		
		Combination singleHead = new Combination(0,"H","T");
		Combination singleTail = new Combination(0,"T","H");
		Combination doubleHead = new Combination(0,"HH","TT");
		Combination doubleTail = new Combination(0,"TT","HH");
		Combination tripleTail = new Combination(0,"TTT","HHH");
		Combination tripleHead = new Combination(0,"HHH","TTT");
		Combination middleHead = new Combination(0,"THT","HTH");
		Combination middleTail = new Combination(0,"HTH","THT");
	
		// EXAMLE RESULT [2, 9, 5, 3, 6, 7, 1, 8, 4]
		Tile t1 = new Tile(1, singleHead,tripleTail,doubleTail,middleTail);
		Tile t2 = new Tile(2, middleTail,doubleHead,singleTail,tripleHead);
		Tile t3 = new Tile(3, singleTail,tripleHead,doubleTail,middleHead);
		
		Tile t4 = new Tile(4, doubleTail,middleHead,tripleHead,middleTail);
		Tile t5 = new Tile(5, singleHead,singleTail,tripleTail,middleHead);
		Tile t6 = new Tile(6, middleTail,doubleHead,tripleTail,singleHead);
		
		Tile t7 = new Tile(7, doubleTail,doubleHead,tripleHead,singleHead);
		Tile t8 = new Tile(8, tripleHead,singleTail,middleHead,doubleTail);
		Tile t9 = new Tile(9, tripleHead,doubleTail,middleHead,singleHead);

		tiles.add(t1);
		tiles.add(t2);
		tiles.add(t3);
		tiles.add(t4);
		tiles.add(t5);
		tiles.add(t6);
		tiles.add(t7);
		tiles.add(t8);
		tiles.add(t9);

	}

}
