package com.quiz.fish;

public class Combination {
	
	private int count;
	private String combination;
	private String match;
	
	public Combination(int count, String combination, String match){
		this.count = count;
		this.combination = combination;
		this.match = match;
	}


	public String getMatch() {
		return match;
	}



	public void setMatch(String match) {
		this.match = match;
	}



	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCombination() {
		return combination;
	}

	public void setCombination(String combination) {
		this.combination = combination;
	}
	
	@Override
	public String toString(){
		return combination;
	}
	
}
