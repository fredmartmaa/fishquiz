package com.quiz.fish;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Result {
	
	public static List<Stack<Integer>> resultArray = new ArrayList<>();

	public Result(Stack<Integer> resultArray) {
		Result.resultArray.add(resultArray);
	}

	public static void printResult() {
		System.out.println("print result");
	}
	
	@Override
	public String toString(){
		return resultArray.toString();
	}
	
}
